from transformers import T5ForConditionalGeneration, T5Tokenizer

def train_model(data):
    tokenizer = T5Tokenizer.from_pretrained("t5-base", max_length=512)
    model = T5ForConditionalGeneration.from_pretrained("t5-base", max_length=512)

    # Train the model using the preprocessed data

    return model
