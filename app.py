import os
import openai
from flask import Flask, request, jsonify, render_template
import pinecone

# Set up OpenAI API credentials
openai.api_key = "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"

# Initialize Pinecone client
pinecone.init(api_key="47e33bed-fbf7-48d3-96fb-905414bf7f41")

app = Flask(__name__)

def generate_response(user_input):
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[
            {"role": "system", "content": "You are a helpful assistant."},
            {"role": "user", "content": user_input}
        ]
    )
    return response.choices[0].message.content

@app.route("/", methods=["GET"])
def home():
    return render_template("index.html")

@app.route("/get_response", methods=["POST"])
def get_response():
    data = request.get_json()
    user_input = data.get("user_input", "")

    bot_response = generate_response(user_input)
    return jsonify({"bot_response": bot_response})

if __name__ == "__main__":
    # Create or get the index
    index_name = "chatbot_index"
    index = pinecone.Index(index_name)

    # Start the web server to handle user requests
    app.run()
