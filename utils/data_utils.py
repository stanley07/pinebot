#import numpy as np
from transformers import T5Tokenizer, T5EncoderModel
import torch
import json

def load_dataset(file_path):
    # Load the dataset from the JSON file
    with open(file_path, "r") as file:
        dataset = json.load(file)
    return dataset

def preprocess_data(dataset):
    preprocessed_data = []

    # Placeholder: Preprocess the dataset
    for item in dataset:
        question = item['question']
        answer = item['answer']

        # Tokenization, cleaning, removing unnecessary characters, etc.
        # Add your preprocessing code here

        # Example: Convert text to lowercase
        question = question.lower()
        answer = answer.lower()

        # Example: Remove leading/trailing whitespaces
        question = question.strip()
        answer = answer.strip()

        # Example: Remove punctuation marks
        import string
        question = question.translate(str.maketrans("", "", string.punctuation))
        answer = answer.translate(str.maketrans("", "", string.punctuation))

        # Create a preprocessed item
        preprocessed_item = {
            'question': question,
            'answer': answer
        }

        preprocessed_data.append(preprocessed_item)

    return preprocessed_data

def generate_vectors(data):
    tokenizer = T5Tokenizer.from_pretrained("t5-base")
    model = T5EncoderModel.from_pretrained("t5-base")

    encoded_inputs = tokenizer.batch_encode_plus(data, padding=True, truncation=True, return_tensors="pt")
    input_ids = encoded_inputs["input_ids"]
    attention_mask = encoded_inputs["attention_mask"]

    with torch.no_grad():
        outputs = model(input_ids, attention_mask=attention_mask)

    embeddings = outputs.last_hidden_state.squeeze().numpy()

    return embeddings