import pinecone

class PineconeClient:
    def __init__(self):
        self.api_key = "47e33bed-fbf7-48d3-96fb-905414bf7f41"
        self.pinecone_index = None

    def create_index(self, index_name):
        pinecone.init(api_key=self.api_key)
        self.pinecone_index = pinecone.Index(index_name)

    def index_documents(self, index_name, documents):
        self.pinecone_index.upsert(items=documents)

    def search(self, index_name, query):
        response = self.pinecone_index.query(queries=[query])
        return response.items
